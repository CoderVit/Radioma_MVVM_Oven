﻿using Oven.Models.Points;
using Oven.Vms;
using System;
using System.Windows;
using System.Windows.Input;

namespace Oven
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            SizeChanged += (sender, args) =>
            {
                var workArea = SystemParameters.WorkArea;
                Left = (workArea.Width - Width) / 2 + workArea.Left; //center of screen
                Top = Math.Abs(Math.Abs(args.NewSize.Height - MinHeight) < 0 ? 0 : (workArea.Height - Height) / 2 + workArea.Top);    //to top or center of screen 
            };
        }

        private Point m_startPoint;
        private bool m_isDragging;

        private void WindowHeader_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!(sender is FrameworkElement element) || Context.HeatenVm.IsProcessRun) return;
            m_isDragging = true;
            m_startPoint = e.GetPosition(element);
            element.CaptureMouse();
            e.Handled = true;
        }

        private void WindowHeader_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!m_isDragging) return;

            var currentPoint = e.GetPosition(null);
            var diffX = currentPoint.X - m_startPoint.X;
            var diffY = currentPoint.Y - m_startPoint.Y;

            Left += diffX;
            Top += diffY;
        }

        private void WindowHeader_OnMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!m_isDragging || !(sender is FrameworkElement element)) return;
            element.ReleaseMouseCapture();
            m_isDragging = false;
            e.Handled = true;
        }

        private MainVm Context => DataContext as MainVm;

        private void Chart_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!Context.IsAdmin) return;
            var point = (DrawPoint)e.GetPosition(Chart);
            PositionTextBox.Text = $"t={point.X}sec  T={point.Y}°C";
        }
    }
}