﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Oven.Builders
{
    public class UdpBuilder
    {
        private IPAddress m_remoteIpAddress;

        private ushort m_txPort;
        private ushort m_rxPort;

        #region Methods

        public UdpBuilder WithIpString(IPAddress ipAddress)
        {
            m_remoteIpAddress = ipAddress;
            return this;
        }

        public UdpBuilder WithPorts(Tuple<ushort, ushort> ports)
        {
            m_txPort = ports.Item1;
            m_rxPort = ports.Item2;
            return this;
        }

        #endregion Methods

        public UdpClient BuildTx()
        {
            var client = new UdpClient(/*6667*/);
            var destEndPoint = new IPEndPoint(m_remoteIpAddress, m_txPort);
            client.Connect(destEndPoint);
            return client;
        }

        public UdpClient BuildRx()
        {
            var client = new UdpClient(/*7776*/);
            var destEndPoint = new IPEndPoint(m_remoteIpAddress, m_rxPort);
            client.Connect(destEndPoint);
            return client;
        }
    }
}