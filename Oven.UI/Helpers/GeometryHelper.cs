﻿using System;
using System.Collections.Generic;
using Oven.Models.Points;

namespace Oven.Helpers
{
    internal class GeometryHelper
    {
        /// <summary>
        /// Calculates points between two points, step of x = 1
        /// create array pass first point in start another space filled distances points, without end of array
        /// </summary>
        /// <param name="drawPoints">array of points</param>
        /// <returns></returns>
        public List<DrawPoint> GetPoints(IList<DrawPoint> drawPoints)
        {
            var points = new List<DrawPoint>();

            for (var drawPointIndex = 0; drawPointIndex < drawPoints.Count; drawPointIndex++)
            {
                var point = drawPoints[drawPointIndex];

                if (drawPointIndex + 1 == drawPoints.Count) //if last element
                {
                    points.Add(point);            //add last point into end of array
                    break;
                }

                var nextPoint = drawPoints[drawPointIndex + 1];

                int timeLength = nextPoint.X - point.X,
                    tempLength = nextPoint.Y - point.Y;

                var slopeAngle = (float)tempLength / timeLength;

                for (var timeIndex = 0; timeIndex < timeLength; timeIndex++)
                {
                    var slopeCoefficient = slopeAngle == 0 ? 0 : tempLength * ((double)timeIndex / timeLength);
                    var x = point.X + timeIndex;
                    var y = (int)Math.Round(point.Y + slopeCoefficient);
                    points.Add(new DrawPoint(x, y));
                }
            }
            //fill to the end zero point
            for (var pointIndex = points.Count; pointIndex < 512; pointIndex++)
            {
                points.Add(new DrawPoint(pointIndex, 0));
            }

            return points;
        }
    }
}