﻿namespace Oven.Helpers
{
    internal interface IChannelHelper
    {
        void Run();
    }
}