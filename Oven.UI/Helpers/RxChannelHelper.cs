﻿using Oven.Annotations;
using Oven.Models.Points;
using Oven.Parsers;
using Oven.Vms;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oven.Helpers
{
    /// <summary>
    /// Class interact from network by udp protocol
    /// Work with haten process in real time
    /// </summary>
    internal class RxChannelHelper
    {
        private readonly UdpClient m_rxClient;
        private readonly HeatenVm m_heatenVm;
        private readonly NotifierVm m_notifierVm;

        private readonly IParser<ProcessPoint> m_parser = new RxParser();

        public RxChannelHelper([NotNull] UdpClient rxClient, [NotNull] HeatenVm heatenVm, [NotNull] NotifierVm notifierVm)
        {
            m_rxClient = rxClient ?? throw new ArgumentNullException(nameof(rxClient));
            m_heatenVm = heatenVm ?? throw new ArgumentNullException(nameof(heatenVm));
            m_notifierVm = notifierVm ?? throw new ArgumentNullException(nameof(notifierVm));
        }

        ~RxChannelHelper()
        {
            m_rxClient.Close(); //free port when app is closing
        }

        public void Run()
        {
            //send ping every time span
            Task.Run(async () =>
            {
                const byte NULL_LENGTH = 0;
                var pingArray = new byte[NULL_LENGTH];
                const ushort PING_TIME = 900;

                for (; ; )
                {
                    try
                    {
                        await m_rxClient.SendAsync(pingArray, NULL_LENGTH);
                    }
                    catch (Exception exception)
                    {
                        m_notifierVm.ShowInfo(exception.Message);
                        continue;
                    }
                    Thread.Sleep(PING_TIME);
                }
            });
            //listen
            Task.Run(async () =>
            {
                for (; ; )
                {
                    const string RESPONSE_FORMAT = "RECV-";

                    UdpReceiveResult response;

                    try
                    {
                        response = await m_rxClient.ReceiveAsync();
                    }
                    catch (Exception exception)
                    {
                        m_notifierVm.ShowInfo(exception.Message);
                        continue;
                    }

                    var responseText = Encoding.ASCII.GetString(response.Buffer);

                    if (string.IsNullOrEmpty(responseText)) //ping
                    {
                        m_heatenVm.Connect(true);
                    }
                    else if (responseText.StartsWith(RESPONSE_FORMAT))  //heaten data response
                    {
                        try
                        {
                            var processPoint = m_parser.ParsePoint(responseText); //split string to mathes array    

                            m_heatenVm.AddPoint(processPoint);
                        }
                        catch (Exception exception)
                        {
                            m_notifierVm.ShowInfo(exception.Message);
                        }
                    }
                }
            });
        }

        public async void SendShowProfile()
        {
            var command = new byte[] { 0x01 };
            await m_rxClient.SendAsync(command, command.Length);
        }

        public async void SendStartProcess()
        {
            var command = new byte[] { 0x02 };
            await m_rxClient.SendAsync(command, command.Length);
        }

        public async void SendStopProcess()
        {
            var command = new byte[] { 0x03 };
            await m_rxClient.SendAsync(command, command.Length);
        }

        public async void SendResetProfile()
        {
            var command = new byte[] { 0x04 };
            await m_rxClient.SendAsync(command, command.Length);
        }
    }
}