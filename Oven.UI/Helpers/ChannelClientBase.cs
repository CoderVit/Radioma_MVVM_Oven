﻿namespace Oven.Helpers
{
    internal abstract class ChannelClientBase
    {
        protected abstract void Start();
        protected abstract void Stop();
    }
}