﻿using Oven.Annotations;
using Oven.Models.Points;
using Oven.Parsers;
using Oven.Vms;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Oven.Helpers
{
    /// <summary>
    /// Class interact from network by udp protocol
    /// Work with controller graph memory
    /// </summary>
    internal class TxChannelHelper
    {
        private readonly UdpClient m_txClient;
        private readonly ControllerVm m_controllerVm;
        private readonly NotifierVm m_notifierVm;

        private readonly IParser<ControllerPoint> m_txHandler = new TxParser();

        private CancellationTokenSource m_tokenSource;

        public TxChannelHelper([NotNull] UdpClient client, [NotNull] ControllerVm controllerVm, [NotNull] NotifierVm notifierVm)
        {
            m_txClient = client ?? throw new ArgumentNullException(nameof(client));
            m_controllerVm = controllerVm ?? throw new ArgumentNullException(nameof(controllerVm));
            m_notifierVm = notifierVm ?? throw new ArgumentNullException(nameof(notifierVm));
        }

        ~TxChannelHelper()
        {
            m_txClient.Close();       //free port when app is closing
        }

        /// <summary>
        /// load graph to controller
        /// </summary>
        /// <param name="points">Collection of oven points</param>
        public async Task LoadGraph(List<DrawPoint> points)
        {
            await Task.Run(async () =>
            {
                const string SUCCESS_STATUS = "OK";
                await m_txClient.SendAsync(new byte[0], 0); //cuz, stop the task which captured is receiveasync

                for (var pointIndex = 0; pointIndex < points.Count; pointIndex++)
                {
                    var currentPoint = points[pointIndex];
                    var pointFormat = $"t={currentPoint.X} T={currentPoint.Y}{Environment.NewLine}";
                    var bytes = Encoding.ASCII.GetBytes(pointFormat);

                    await m_txClient.SendAsync(bytes, bytes.Length);

                    var response = await m_txClient.ReceiveAsync();
                    var status = Encoding.ASCII.GetString(response.Buffer);

                    if (status != SUCCESS_STATUS) throw new FormatException("Ошибка загрузки...");    //check confirm status

                    response = await m_txClient.ReceiveAsync();
                    var confirmText = Encoding.ASCII.GetString(response.Buffer);

                    var parsedPoint = m_txHandler.ParsePoint(confirmText);

                    if (currentPoint.X != parsedPoint.X || currentPoint.Y != parsedPoint.Y) throw new FormatException("Координаты на совпадают");   //check coordinate match
                    m_controllerVm.AddPoint(parsedPoint);
                    Thread.Sleep(5);
                }
            });
        }

        /// <summary>
        /// Start tasks for listenning and receiving rx port data
        /// </summary>
        public void Run()
        {
            m_tokenSource = new CancellationTokenSource();
            var token = m_tokenSource.Token;
            //ping send
            Task.Run(async () =>
            {
                const byte NULL_LENGTH = 0;
                var pingArray = new byte[NULL_LENGTH];
                const ushort PING_TIME = 900;

                for (; ; )
                {
                    if (token.IsCancellationRequested) break;
                    try
                    {
                        await m_txClient.SendAsync(pingArray, NULL_LENGTH);
                    }
                    catch (Exception exception)
                    {
                        m_notifierVm.ShowInfo(exception.Message);
                        continue;
                    }
                    Thread.Sleep(PING_TIME);
                }
            }, token);
            //listen
            Task.Run(async () =>
            {
                const string RESPONSE_FORMAT = "SEND-";
                for (; ; )
                {
                    if (token.IsCancellationRequested) break;
                    UdpReceiveResult response;

                    try
                    {
                        response = await m_txClient.ReceiveAsync();
                    }
                    catch (Exception exception)
                    {
                        m_notifierVm.ShowInfo(exception.Message);
                        continue;
                    }

                    var responseText = Encoding.ASCII.GetString(response.Buffer);
                    if (string.IsNullOrEmpty(responseText)) //ping response
                    {

                    }
                    else if (responseText.StartsWith(RESPONSE_FORMAT)) //reset profile response
                    {
                        try
                        {
                            var parsedPoint = m_txHandler.ParsePoint(responseText);
                            m_controllerVm.UpdatePoint(parsedPoint);
                        }
                        catch (Exception exception)
                        {
                            m_notifierVm.ShowInfo(exception.Message);
                        }
                    }
                }
            }, token);
        }
        /// <summary>
        /// Cancellation token, managed by loop send listen of tx channel
        /// </summary>
        public void Stop()
        {
            m_tokenSource.Cancel();
        }
    }
}