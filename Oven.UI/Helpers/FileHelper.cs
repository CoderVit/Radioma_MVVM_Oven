﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Oven.Models;
using Oven.Models.Points;

namespace Oven.Helpers
{
    internal class FileHelper
    {
        private const string POINT_TAG = "points:";

        public void SaveProfile(IList<DrawPoint> points, string fileName)
        {
            var builder = new StringBuilder(POINT_TAG);

            for (var pointIndex = 0; pointIndex < points.Count; pointIndex++)
            {
                var point = points[pointIndex];
                builder.AppendFormat($"{point.X},{point.Y};");
            }
            File.WriteAllText(fileName, builder.ToString());
        }

        public DrawPoint[] LoadProfile(string path)
        {
            var text = File.ReadAllText(path);

            var matchCollection = Regex.Matches(text, @"(?:points:)?(\d+),(\d+);");
            var points = new DrawPoint[matchCollection.Count];

            for (var matchIndex = 0; matchIndex < matchCollection.Count; matchIndex++)
            {
                var match = matchCollection[matchIndex];
                var x = Convert.ToInt32(match.Groups[1].Value);
                var y = Convert.ToInt32(match.Groups[2].Value);
                points[matchIndex] = new DrawPoint(x, y);
            }

            return points;
        }
    }
}