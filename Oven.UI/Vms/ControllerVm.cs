﻿using Oven.Annotations;
using Oven.Models.Lines;
using Oven.Models.Points;
using Oven.mvvm;
using System;
using System.Collections.ObjectModel;

namespace Oven.Vms
{
    internal class ControllerVm : ModelBase
    {
        private readonly NotifierVm m_notifier;
        private readonly ControllerModel m_model = new ControllerModel();

        private Action<ControllerPoint> m_handler;

        public ControllerVm([NotNull] NotifierVm notifier)
        {
            m_notifier = notifier ?? throw new ArgumentNullException(nameof(notifier));

            m_model.OnGraphConfirmed += OnGraphConfirmed;
            m_model.PropertyChanged += (sender, args) => RaisePropertyChanged(args.PropertyName);
        }

        private void OnGraphConfirmed()
        {
            m_model.IsControllerBusy = false;
            m_notifier.ShowWarnInlineText("пайка");
            OnReadyToProcess?.Invoke();
        }

        #region Delegates

        public Action OnReadyToProcess;

        #endregion Delegates

        #region Methods

        public void Busy(bool statement)
        {
            m_model.IsControllerBusy = statement;
        }

        public void AddPoint(ControllerPoint point)
        {
            m_model.AddPoint(point);
        }

        public void ShowProfile()
        {
            m_handler = m_model.ConfirmPoint;
        }

        public void ConfirmProfile()
        {
            m_model.IsGraphReseted = false;
            m_model.IsControllerBusy = true;
            m_notifier.ShowInlineText("Проверка графика");
            m_handler = m_model.CheckPoint;
        }

        public void ResetProfile()
        {
            m_model.IsGraphConfirmed = false;
            m_handler = m_model.ResetPoint;
        }

        public void UpdatePoint(ControllerPoint point)
        {
            try
            {
                m_handler?.Invoke(point);
            }
            catch (Exception exception)
            {
                m_notifier.ShowInfo(exception.Message);
            }
        }

        public void Clear()
        {
            m_model.IsGraphConfirmed = false;
            m_model.IsGraphReseted = false;
            m_model.Clear();
        }

        #endregion Methods

        #region Properties

        public bool IsControllerBusy => m_model.IsControllerBusy;
        public bool IsGraphReseted => m_model.IsGraphReseted;
        public bool IsGraphConfirmed => m_model.IsGraphConfirmed;
        public ReadOnlyCollection<ControlllerLine> ControllerGraph => m_model.ControllerGraph;

        #endregion Properties
    }
}