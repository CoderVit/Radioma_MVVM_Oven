﻿using Oven.Annotations;
using Oven.Models.Lines;
using Oven.Models.Points;
using Oven.mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;

namespace Oven.Vms
{
    internal class HeatenVm : ModelBase
    {
        private readonly NotifierVm m_notifierVm;
        private readonly Timer m_timer;
        private Timer m_heatenTimer;

        public HeatenVm([NotNull] NotifierVm notifierVm)
        {
            m_notifierVm = notifierVm ?? throw new ArgumentNullException(nameof(notifierVm));

            m_timer = new Timer(Callback, null, TimeSpan.Zero, TimeSpan.FromMilliseconds(600));
            m_points = new List<ProcessPoint>();
        }

        private void Callback(object state)
        {
            IsDeviceConnected = false;
            RaisePropertyChanged(nameof(IsDeviceConnected));
        }

        private void HeatenTimer_Callback(object state)
        {
            m_notifierVm.ShowInfo("Процесс завершен");
            IsProcessDone = true;
            m_heatenTimer.Dispose();
        }

        #region Methods

        public void Process(bool statement)
        {
            IsProcessRun = statement;
            RaisePropertyChanged(nameof(IsProcessRun));
        }

        public void Connect(bool statement)
        {
            m_timer.Change(TimeSpan.FromMilliseconds(1500), Timeout.InfiniteTimeSpan);
            if (IsDeviceConnected == statement) return;
            IsDeviceConnected = statement;
            RaisePropertyChanged(nameof(IsDeviceConnected));
        }

        public void AddPoint(ProcessPoint point)
        {
            if (IsProcessRun)         //if program started when process already invoked   
            {
                m_heatenTimer.Change(TimeSpan.FromSeconds(3), TimeSpan.Zero);
            }
            else
            {
                Process(true);
                m_heatenTimer = new Timer(HeatenTimer_Callback, null, TimeSpan.FromSeconds(3), TimeSpan.Zero);
            }

            m_points.Add(point);
            RaisePropertyChanged(nameof(ProcessGraph));
        }

        public void Clear()
        {
            m_points.Clear();
            RaisePropertyChanged(nameof(ProcessGraph));
        }

        #endregion Methods

        #region Properties

        public bool IsDeviceConnected { get; private set; }
        public bool IsProcessRun { get; private set; }

        private bool m_isProcessDone;

        public bool IsProcessDone
        {
            get => m_isProcessDone;
            set
            {
                m_isProcessDone = value;
                RaisePropertyChanged();
            }
        }

        private readonly IList<ProcessPoint> m_points;
        public ReadOnlyCollection<ProcessLine> ProcessGraph
        {
            get
            {
                var lines = new Collection<ProcessLine>();

                for (var pointIndex = 1; pointIndex < m_points.Count; pointIndex++)
                {
                    var previosPoint = m_points[pointIndex - 1];
                    var point = m_points[pointIndex];

                    lines.Add(new ProcessLine(previosPoint, point));
                }

                return new ReadOnlyCollection<ProcessLine>(lines);
            }
        }

        #endregion Properties
    }
}