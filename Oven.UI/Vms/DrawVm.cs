﻿using Oven.Models.Lines;
using Oven.Models.Points;
using Oven.mvvm;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Oven.Vms
{
    internal class DrawVm : ModelBase
    {
        private readonly DrawModel m_model = new DrawModel();

        public DrawVm()
        {
            m_model.PropertyChanged += (sender, args) => { RaisePropertyChanged(args.PropertyName); };
        }

        #region Methods

        public void AddPoint(DrawPoint point)
        {
            m_model.AddPoint(point);
        }

        public void AddMiddlePoint(DrawPoint point)
        {
            m_model.AddMiddlePoint(point);
        }

        public void Clear()
        {
            if (m_model.DrawPoints.Count == 1) return;
            m_model.Clear();
        }

        #endregion Methods

        #region Commands

        public ICommand NewGraphCommand => new RelayCommand(Clear);

        #endregion Commands

        #region Properties

        public ReadOnlyObservableCollection<DrawPoint> DrawPoints => m_model.DrawPoints;
        public ReadOnlyCollection<DrawLine> DrawLines => m_model.DrawLines;

        #endregion Properties
    }
}