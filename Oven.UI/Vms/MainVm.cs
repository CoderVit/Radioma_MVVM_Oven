﻿using Oven.Annotations;
using Oven.Manager;
using Oven.Models.Lines;
using Oven.Models.Points;
using Oven.mvvm;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Oven.Vms
{
    internal class MainVm : ModelBase
    {
        private readonly MainModel m_model = new MainModel();

        public NetworkManager NetworkManager { get; }
        public FileManager FileManager { get; }
        public AppManager AppManager { get; } = new AppManager();

        public DrawVm DrawViewModel { get; }
        public ControllerVm ControllerVm { get; }
        public HeatenVm HeatenVm { get; }
        public NotifierVm NotifierVm { get; }

        public MainVm([NotNull] NetworkManager networkManager)
        {
            NetworkManager = networkManager ?? throw new ArgumentNullException(nameof(networkManager));

            m_model.PropertyChanged += ModelOnPropertyChanged;

            DrawViewModel = NetworkManager.DrawVm;
            DrawViewModel.PropertyChanged += ModelOnPropertyChanged;

            ControllerVm = NetworkManager.ControllerVm;
            ControllerVm.PropertyChanged += ModelOnPropertyChanged;

            HeatenVm = NetworkManager.HeatenVm;
            HeatenVm.PropertyChanged += ModelOnPropertyChanged;

            NotifierVm = NetworkManager.NotifierVm;
            NotifierVm.PropertyChanged += ModelOnPropertyChanged;

            FileManager = new FileManager(DrawViewModel, NotifierVm);
        }

        private void ModelOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            RaisePropertyChanged(args.PropertyName);
        }


        #region Commands

        public ICommand AddPointCommand => new ClickCommand(DrawViewModel.AddPoint, o => ControllerGraph.Count == 0 && HeatenVm.ProcessGraph.Count == 0);
        public ICommand AddMiddlePointCommand => new ClickCommand(DrawViewModel.AddMiddlePoint, o => ControllerGraph.Count == 0 && HeatenVm.ProcessGraph.Count == 0);

        public ICommand StateToggleCommand => new RelayCommand(m_model.StateToggle);
        public ICommand ResizeToggleCommand => new RelayCommand(m_model.ResizeToggle);

        public ICommand ToggleAdminCommand => new RelayCommand(m_model.AdminToggle);

        #endregion Commands

        #region Properties

        public WindowState WindowState
        {
            get => m_model.WindowState;
            set => m_model.StateToggle();
        }

        public double Width
        {
            get => m_model.Width;
            set { }
        }
        public double Height
        {
            get => m_model.Height;
            set { }
        }

        public bool IsMaximized => m_model.IsMaximized;
        public bool IsAdmin => m_model.IsAdmin;

        public double ZoomX => m_model.ZoomX;
        public double ZoomY => m_model.ZoomY;

        public ReadOnlyObservableCollection<DrawPoint> DrawPoints => DrawViewModel.DrawPoints;
        public ReadOnlyCollection<DrawLine> DrawLines => DrawViewModel.DrawLines;

        public ReadOnlyCollection<ControlllerLine> ControllerGraph => ControllerVm.ControllerGraph;
        public ReadOnlyCollection<ProcessLine> ProcessGraph => HeatenVm.ProcessGraph;

        #endregion Properties
    }
}