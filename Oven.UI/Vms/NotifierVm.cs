﻿using Oven.mvvm;
using System.Windows.Input;

namespace Oven.Vms
{
    internal class NotifierVm : ModelBase
    {
        #region Methods

        public void ShowWarnInlineText(string text)
        {
            IsWarn = true;
            RaisePropertyChanged(nameof(IsWarn));
            ShowInlineText(text);
        }

        public void ShowInlineText(string text)
        {
            InlineMessage = text;
            RaisePropertyChanged(nameof(InlineMessage));
        }

        public void ShowInfo(string message)
        {
            IsShowing = true;
            RaisePropertyChanged(nameof(IsShowing));

            Message = message;
            RaisePropertyChanged(nameof(Message));

            IsWarn = false;
            RaisePropertyChanged(nameof(IsWarn));
        }

        private void Close()
        {
            IsShowing = false;
            Message = string.Empty;
            RaisePropertyChanged(nameof(Message));
            RaisePropertyChanged(nameof(IsShowing));
        }

        #endregion Methods 

        #region Commands

        public ICommand CloseCommand => new RelayCommand(Close);

        #endregion Commands

        #region Properties

        public bool IsShowing { get; private set; }
        public string Message { get; private set; }
        public string InlineMessage { get; private set; }
        public bool IsWarn { get; private set; }

        #endregion Properties
    }
}