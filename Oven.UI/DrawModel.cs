﻿using Oven.Models.Lines;
using Oven.Models.Points;
using Oven.mvvm;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Oven.Extensions;

namespace Oven
{
    internal class DrawModel : ModelBase
    {
        private readonly DrawPoint m_firstPoint = new DrawPoint(0, 20, false);

        public DrawModel()
        {
            m_points = new ObservableCollection<DrawPoint>(new[] { m_firstPoint });  //put unchageable and unmoved first point
            DrawPoints = new ReadOnlyObservableCollection<DrawPoint>(m_points);
        }

        /// <summary>
        /// add point to point collection
        /// </summary>
        /// <param name="point">point to add</param>
        public void AddPoint(DrawPoint point)
        {
            if (!point.IsValid()) return;
            point.PropertyChanged += PointOnPropertyChanged;
            point.OnDeleteInvoke += DeletePoint;

            m_points.Add(point);
            ValidatePoints();
            RaisePropertyChanged(nameof(DrawPoints));
            RaisePropertyChanged(nameof(DrawLines));
        }

        /// <summary>
        /// add point between existing points
        /// </summary>
        /// <param name="point">point to add</param>
        public void AddMiddlePoint(DrawPoint point)
        {
            for (byte pointIndex = 0; pointIndex < m_points.Count; pointIndex++)
            {
                var current = m_points[pointIndex];
                if (current.X < point.X) continue;
                point.PropertyChanged += PointOnPropertyChanged;
                point.OnDeleteInvoke += DeletePoint;
                m_points.Insert(pointIndex, point);
                RaisePropertyChanged(nameof(DrawPoints));
                RaisePropertyChanged(nameof(DrawLines));
                break;
            }
        }
        private void PointOnPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == nameof(DrawPoint.X)) ValidatePoints();
            RaisePropertyChanged(nameof(DrawPoints));
            RaisePropertyChanged(nameof(DrawLines));
        }

        /// <summary>
        /// clear collection, except first element
        /// </summary>
        public void Clear()
        {
            m_points.Clear();

            m_points.Add(m_firstPoint);

            RaisePropertyChanged(nameof(DrawPoints));
            RaisePropertyChanged(nameof(DrawLines));
        }

        private void DeletePoint(DrawPoint point)
        {
            m_points.Remove(point);
            point.PropertyChanged -= PointOnPropertyChanged;
            ValidatePoints();
            RaisePropertyChanged(nameof(DrawPoints));
            RaisePropertyChanged(nameof(DrawLines));
        }

        /// <summary>
        /// valid poitns for correct
        /// calculate points 'x' position on true draw
        /// </summary>
        private void ValidatePoints()
        {
            for (var pointIndex = 1; pointIndex < m_points.Count; pointIndex++)
            {
                var previousPoint = m_points[pointIndex - 1];
                var point = m_points[pointIndex];

                point.Error(point.X < previousPoint.X);
            }
        }

        #region Properties

        public bool IsCanDraw { get; private set; }

        private readonly ObservableCollection<DrawPoint> m_points;
        public readonly ReadOnlyObservableCollection<DrawPoint> DrawPoints;

        /// <summary>
        /// Lines collection property, create line array if points collection changed
        /// </summary>
        public ReadOnlyCollection<DrawLine> DrawLines
        {
            get
            {
                var lines = new Collection<DrawLine>();

                for (var pointIndex = 1; pointIndex < m_points.Count; pointIndex++)
                {
                    var previosPoint = m_points[pointIndex - 1];
                    var point = m_points[pointIndex];

                    lines.Add(new DrawLine(previosPoint, point));
                }

                return new ReadOnlyCollection<DrawLine>(lines);
            }
        }
    }

    #endregion Properties
}