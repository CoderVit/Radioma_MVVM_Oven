﻿using Oven.Annotations;
using Oven.Builders;
using Oven.Helpers;
using Oven.Manager;
using Oven.Vms;
using System;
using System.Windows;

namespace Oven
{
    public partial class App : Application
    {
        private readonly UdpBuilder m_udpBuilder;

        private readonly NotifierVm m_notifierVm = new NotifierVm();

        public App([NotNull] UdpBuilder builder)
        {
            m_udpBuilder = builder ?? throw new ArgumentNullException(nameof(builder));
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            var heatenVm = new HeatenVm(m_notifierVm);
            var controllerVm = new ControllerVm(m_notifierVm);

            var txChannelHelper = new TxChannelHelper(m_udpBuilder.BuildTx(), controllerVm, m_notifierVm);
            var rxChannelHelper = new RxChannelHelper(m_udpBuilder.BuildRx(), heatenVm, m_notifierVm);

            var networkManager = new NetworkManager(txChannelHelper, rxChannelHelper, controllerVm, heatenVm, m_notifierVm);

            var window = new MainWindow
            {
                DataContext = new MainVm(networkManager)
            };

            window.Show();

            base.OnStartup(e);
        }
    }
}