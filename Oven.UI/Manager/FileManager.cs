﻿using Microsoft.Win32;
using Oven.Annotations;
using Oven.Helpers;
using Oven.mvvm;
using Oven.Vms;
using System;
using System.Windows.Input;
using Oven.Extensions;

namespace Oven.Manager
{
    internal class FileManager
    {
        private readonly DrawVm m_drawVm;
        private readonly NotifierVm m_notifierVm;

        private readonly FileHelper m_fileHelper = new FileHelper();

        private const string DIALOG_FILTER = "Файлы конфигурации печки | *.ovn";

        public FileManager([NotNull] DrawVm drawVm, [NotNull] NotifierVm notifierVm)
        {
            m_drawVm = drawVm ?? throw new ArgumentNullException(nameof(drawVm));
            m_notifierVm = notifierVm ?? throw new ArgumentNullException(nameof(notifierVm));
        }

        #region Methods

        private void Open()
        {
            var saveDialog = new OpenFileDialog
            {
                Filter = DIALOG_FILTER
            };
            var result = saveDialog.ShowDialog();
            if (!result.HasValue || !result.Value) return;

            var points = m_fileHelper.LoadProfile(saveDialog.FileName);

            if (points[0].X != 0 || points[0].Y != 20)
            {
                m_notifierVm.ShowInfo("Первая точка должна быть в формате t=0 T=20");
                return;
            }

            m_drawVm.Clear();

            for (var pointIndex = 1; pointIndex < points.Length; pointIndex++)  //start step 1, cuz duplucated 0,20 value
            {
                var point = points[pointIndex];
                if (!point.IsValid())
                {
                    m_notifierVm.ShowInfo("Координата должна быть в диапазоне t=0-511 T=0-350");
                    break;
                }
                m_drawVm.AddPoint(point);
            }
        }

        private void Write()
        {
            var fileDialog = new SaveFileDialog
            {
                Filter = DIALOG_FILTER
            };
            var result = fileDialog.ShowDialog();
            if (!result.HasValue || !result.Value) return;

            try
            {
                m_fileHelper.SaveProfile(m_drawVm.DrawPoints, fileDialog.FileName);
            }
            catch (Exception exception)
            {
                m_notifierVm.ShowInfo(exception.Message);
            }
        }

        #endregion Methods

        #region Commands

        public ICommand OpenFileCommand => new RelayCommand(Open);
        public ICommand SaveFileCommand => new RelayCommand(Write);

        #endregion Commands
    }
}
