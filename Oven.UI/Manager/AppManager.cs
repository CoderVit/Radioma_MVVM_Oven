﻿using Oven.mvvm;
using System;
using System.Windows.Input;

namespace Oven.Manager
{
    internal class AppManager
    {
        #region Methods

        private void ExitApp()
        {
            const byte NOT_ERROR_CODE = 0;
            Environment.Exit(NOT_ERROR_CODE);
        }

        #endregion Methods

        #region Commands

        public ICommand ExitAppCommand => new RelayCommand(ExitApp);

        #endregion Commands
    }
}