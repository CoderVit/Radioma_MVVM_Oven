﻿using Oven.Annotations;
using Oven.Helpers;
using Oven.mvvm;
using Oven.Vms;
using System;
using System.Linq;
using System.Windows.Input;

namespace Oven.Manager
{
    internal class NetworkManager : ModelBase
    {
        private readonly TxChannelHelper m_txChannelHelper;
        private readonly RxChannelHelper m_rxChannelHelper;
        private readonly GeometryHelper m_geometryHelper = new GeometryHelper();

        public NetworkManager([NotNull] TxChannelHelper txChannelHelper, [NotNull] RxChannelHelper rxChannelHelper,
            [NotNull] ControllerVm controllerVm, [NotNull] HeatenVm heatenVm, [NotNull] NotifierVm notifierVm)
        {
            m_txChannelHelper = txChannelHelper ?? throw new ArgumentNullException(nameof(txChannelHelper));
            m_txChannelHelper.Run();

            m_rxChannelHelper = rxChannelHelper ?? throw new ArgumentNullException(nameof(rxChannelHelper));
            m_rxChannelHelper.Run();

            ControllerVm = controllerVm ?? throw new ArgumentNullException(nameof(controllerVm));
            ControllerVm.OnReadyToProcess += OnReadyToProcess;

            HeatenVm = heatenVm ?? throw new ArgumentNullException(nameof(heatenVm));
            NotifierVm = notifierVm ?? throw new ArgumentNullException(nameof(notifierVm));
        }

        private void OnReadyToProcess()
        {
            //HeatenVm.Process(true);
            m_rxChannelHelper.SendStartProcess();
        }

        #region Methods

        /// <summary>
        /// Load graph to 'Device'
        /// </summary>
        private async void LoadGraph()
        {
            if (ControllerVm.ControllerGraph.Count != 0)
            {
                NotifierVm.ShowInfo("График уже загружен");
                return;
            }

            var drawPoints = DrawVm.DrawPoints;

            if (drawPoints.Any(point => point.IsError))
            {
                NotifierVm.ShowInfo("Некорректный график");
                return;
            }

            ControllerVm.Busy(true);
            try
            {
                m_txChannelHelper.Stop();
                var points = m_geometryHelper.GetPoints(drawPoints);
                await m_txChannelHelper.LoadGraph(points);
            }
            catch (Exception exception)
            {
                NotifierVm.ShowInfo(exception.Message);
            }
            finally
            {
                ControllerVm.Busy(false);
                m_txChannelHelper.Run();
            }
        }

        /// <summary>
        /// Clear graph on display
        /// </summary>
        private void ClearGraph()
        {
            ControllerVm.Clear();
        }

        /// <summary>
        /// Show controller loaded profile
        /// </summary>
        private void ShowProfile()
        {
            ControllerVm.Busy(true); //after got a 511 package of response, busy status will changed to false
            ControllerVm.ShowProfile();
            m_rxChannelHelper.SendShowProfile();
        }

        /// <summary>
        /// Validate the profile, and start process if success
        /// </summary>
        private void StartProcess()
        {
            if (HeatenVm.IsProcessRun) return;

            ControllerVm.ConfirmProfile();
            m_rxChannelHelper.SendShowProfile();
        }
        /// <summary>
        /// Stop process, set status to done
        /// </summary>
        private void StopProcess()
        {
            if (!HeatenVm.IsProcessRun) return;
            HeatenVm.IsProcessDone = true;
            m_rxChannelHelper.SendStopProcess();
        }
        /// <summary>
        /// Reset loaded profile, to 'x' = 0
        /// </summary>
        private void ResetProfile()
        {
            ControllerVm.Busy(true);    //after got a 511 package of response, busy status will changed to false
            ControllerVm.ResetProfile();
            m_rxChannelHelper.SendResetProfile();   //send command to reset profile
        }

        #endregion Methods

        #region Commands

        public ICommand LoadGraphCommand => new RelayCommand(LoadGraph);
        public ICommand ClearGraphCommand => new RelayCommand(ClearGraph);
        public ICommand ShowProfileCommand => new RelayCommand(ShowProfile);
        public ICommand StartProcessCommand => new RelayCommand(StartProcess);
        public ICommand StopProcessCommand => new RelayCommand(StopProcess);
        public ICommand ResetProfileCommand => new RelayCommand(ResetProfile);

        #endregion Commands

        #region Properties

        public DrawVm DrawVm { get; } = new DrawVm();
        public ControllerVm ControllerVm { get; }
        public HeatenVm HeatenVm { get; }
        public NotifierVm NotifierVm { get; }

        #endregion Properties
    }
}