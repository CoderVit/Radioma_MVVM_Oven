﻿using Oven.Models.Points;

namespace Oven.Extensions
{
    public static class Extension
    {
        private const byte MIN_RANGE = 0;
        private const ushort X_MAX = 511;
        private const ushort Y_MAX = 350;

        internal static bool IsValid(this DrawPoint point)
        {
            return MIN_RANGE <= point.X && point.X <= X_MAX && MIN_RANGE <= point.Y && point.Y <= Y_MAX;
        }
    }
}