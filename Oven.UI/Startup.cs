﻿using System;
using System.Net;
using System.Windows;
using Oven.Builders;
using Oven.Models;
using Oven.Properties;

namespace Oven
{
    public class Startup
    {
        [STAThread]
        public static void Main(string[] args)
        {
            try
            {
                var settings = Settings.Default;

                var ip = IPAddress.Parse(settings.RemoteIp);

                var config = new Config(ip, settings.RemoteTxPort, settings.RemoteRxPort);

                var udpBuilder = new UdpBuilder()
                    .WithIpString(config.IpAddress)
                    .WithPorts(new Tuple<ushort, ushort>(config.TxPort, config.RxPort));

                var app = new App(udpBuilder);
                app.Run();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}