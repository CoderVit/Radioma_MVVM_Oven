﻿namespace Oven.Models
{
    internal class ProcessTemperature
    {
        public ProcessTemperature(int temperature, bool isPrimary)
        {
            Temperature = temperature;
            IsPrimary = isPrimary;
        }

        public int Temperature { get; }
        public bool IsPrimary { get; }
    }
}