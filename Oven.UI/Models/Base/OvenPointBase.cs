﻿using Oven.mvvm;

namespace Oven.Models.Base
{
    internal class OvenPointBase : ModelBase
    {
        public OvenPointBase(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; set; }

        public int Y { get; set; }
    }
}