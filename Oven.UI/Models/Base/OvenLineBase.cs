﻿using Oven.mvvm;

namespace Oven.Models.Base
{
    internal class OvenLineBase : ModelBase
    {
        public OvenLineBase(OvenPointBase startPoint, OvenPointBase endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        #region Properties
        public OvenPointBase StartPoint { get; }
        public OvenPointBase EndPoint { get; }
        #endregion Properties
    }
}