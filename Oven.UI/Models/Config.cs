﻿using System.Net;

namespace Oven.Models
{
    public struct Config
    {
        public Config(IPAddress ipAddress, ushort txPort, ushort rxPort)
        {
            IpAddress = ipAddress;
            TxPort = txPort;
            RxPort = rxPort;
        }

        public IPAddress IpAddress { get; }
        public ushort TxPort { get; }
        public ushort RxPort { get; }
    }
}