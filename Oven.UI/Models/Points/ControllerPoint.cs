﻿using Oven.Models.Base;

namespace Oven.Models.Points
{
    internal class ControllerPoint : OvenPointBase
    {
        public ControllerPoint(int x, int y) : base(x, y) { }

        #region Properties

        public bool IsConfirmed { get; set; }

        #endregion Properties
    }
}