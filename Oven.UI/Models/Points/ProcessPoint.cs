﻿namespace Oven.Models.Points
{
    internal class ProcessPoint
    {
        public ProcessPoint(int x, ProcessTemperature firstSensor, ProcessTemperature secondSensor, ProcessTemperature thirdSensor)
        {
            X = x;
            FirstSensor = firstSensor;
            SecondSensor = secondSensor;
            ThirdSensor = thirdSensor;
        }

        public int X { get; }

        public ProcessTemperature FirstSensor { get; }
        public ProcessTemperature SecondSensor { get; }
        public ProcessTemperature ThirdSensor { get; }
    }
}