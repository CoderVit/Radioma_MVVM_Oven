﻿using Oven.Models.Base;
using System;
using System.Windows;

namespace Oven.Models.Points
{
    internal class DrawPoint : OvenPointBase
    {
        public DrawPoint(int x, int y, bool isCanDrag = true) : base(x, y)
        {
            IsDraggable = isCanDrag;
        }

        #region Delegates

        public Action<DrawPoint> OnDeleteInvoke;

        #endregion Delegates

        #region Methods

        public void Error(bool statement)
        {
            IsError = statement;
            RaisePropertyChanged(nameof(IsError));
        }

        public void SetX(int x)
        {
            X = x;
            RaisePropertyChanged(nameof(X));
        }

        public void SetY(int y)
        {
            Y = y;
            RaisePropertyChanged(nameof(Y));
        }

        #endregion Methods

        #region Properties

        public bool IsDraggable { get; }

        public bool IsError { get; private set; }

        #endregion Properties

        #region Static Convertors

        public static explicit operator DrawPoint(Point point)
        {
            return new DrawPoint((int)point.X, (int)point.Y);
        }

        #endregion Static Convertors
    }
}