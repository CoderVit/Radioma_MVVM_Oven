﻿using Oven.Models.Base;

namespace Oven.Models.Lines
{
    internal class DrawLine : OvenLineBase
    {
        public DrawLine(OvenPointBase startPoint, OvenPointBase endPoint) : base(startPoint, endPoint)
        {
            IsError = endPoint.X < startPoint.X;
            IsFrozen = endPoint.Y < startPoint.Y;
        }

        #region Properties
        public bool IsError { get; }
        public bool IsFrozen { get; }
        #endregion Properties
    }
}