﻿using Oven.Models.Base;
using Oven.Models.Points;

namespace Oven.Models.Lines
{
    internal class ControlllerLine : OvenLineBase
    {
        public ControlllerLine(ControllerPoint startPoint, ControllerPoint endPoint) : base(startPoint, endPoint)
        {
            IsConfirmed = startPoint.IsConfirmed && endPoint.IsConfirmed;
        }

        #region Properties

        public bool IsConfirmed { get; }

        #endregion Properties
    }
}