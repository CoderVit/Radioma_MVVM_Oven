﻿using Oven.Models.Points;

namespace Oven.Models.Lines
{
    internal class ProcessLine
    {
        public ProcessLine(ProcessPoint startPoint, ProcessPoint endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        public ProcessPoint StartPoint { get; }
        public ProcessPoint EndPoint { get; }
    }
}