﻿using Oven.mvvm;
using System;
using System.Windows;

namespace Oven
{
    internal class MainModel : ModelBase
    {
        private const double MIN_WIDTH = 800;
        private const double MIN_HEIGHT = 650;
        private const double SIZE_COEFFICIENT = 1.33;

        private const ushort X_COEFFICIENT = 523;
        private const ushort Y_COEFFICIENT = 386;

        private const double X_MIN_ZOOM = 1.5;
        private const double Y_MIN_ZOOM = -1.6;

        #region Methods
        //Minimize/Restore window
        public void StateToggle()
        {
            WindowState = WindowState == WindowState.Normal ? WindowState.Minimized : WindowState.Normal;
            RaisePropertyChanged(nameof(WindowState));
        }

        public void ResizeToggle()
        {
            //TODO Remove properties duplication
            if (IsMaximized)
            {
                Height = MIN_HEIGHT;
                RaisePropertyChanged(nameof(Height));
                Width = MIN_WIDTH;
                RaisePropertyChanged(nameof(Width));
                ZoomY = Y_MIN_ZOOM;
                RaisePropertyChanged(nameof(ZoomY));
                ZoomX = X_MIN_ZOOM;
                RaisePropertyChanged(nameof(ZoomX));
            }
            else
            {
                Height = SystemParameters.WorkArea.Height; //get height screen size
                RaisePropertyChanged(nameof(Height));
                Width = Height * SIZE_COEFFICIENT; //Multiplication width to relative Height coefficient
                RaisePropertyChanged(nameof(Width));
                //Devides zooms to coefficients
                ZoomY = -Height / Y_COEFFICIENT;
                RaisePropertyChanged(nameof(ZoomY));
                ZoomX = Width / X_COEFFICIENT;
                RaisePropertyChanged(nameof(ZoomX));
            }

            RaisePropertyChanged(nameof(IsMaximized));
        }

        public void AdminToggle()
        {
            IsAdmin = !IsAdmin;
            RaisePropertyChanged(nameof(IsAdmin));
        }

        #endregion Methods

        #region Properties

        public WindowState WindowState { get; private set; }
        public double Width { get; private set; } = MIN_WIDTH;
        public double Height { get; private set; } = MIN_HEIGHT;
        public double ZoomX { get; private set; } = X_MIN_ZOOM;
        public double ZoomY { get; private set; } = Y_MIN_ZOOM;
        public bool IsMaximized => Math.Abs(Width - MIN_WIDTH) > 0;
        public bool IsAdmin { get; private set; }

        #endregion Properties
    }
}