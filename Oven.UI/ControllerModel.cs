﻿using Oven.Models.Lines;
using Oven.Models.Points;
using Oven.mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Oven
{
    internal class ControllerModel : ModelBase
    {
        private const ushort LAST_POINT = 511;

        #region Delegates

        public Action OnGraphConfirmed;

        #endregion Delegates

        #region Methods

        public void AddPoint(ControllerPoint point)
        {
            m_points.Add(point);
            RaisePropertyChanged(nameof(ControllerGraph));
        }
        /// <summary>
        /// Confirm profile before starting a process
        /// </summary>
        /// <param name="point"></param>
        public void CheckPoint(ControllerPoint point)
        {
            var currentPoint = m_points.FirstOrDefault(ovenPoint => ovenPoint.X == point.X && ovenPoint.Y == point.Y);
            if (currentPoint == null) return;

            currentPoint.IsConfirmed = true;
            RaisePropertyChanged(nameof(ControllerGraph));

            if (currentPoint.X != LAST_POINT) return;
            if (m_points.Any(controllerPoint => !controllerPoint.IsConfirmed)) throw new Exception("Профиль загруженный в контроллер не соответствует текущему.");
            OnGraphConfirmed();
        }
        /// <summary>
        /// Show Profile on Screen
        /// </summary>
        /// <param name="point"></param>
        public void ConfirmPoint(ControllerPoint point)
        {
            var currentPoint = m_points.FirstOrDefault(ovenPoint => ovenPoint.X == point.X && ovenPoint.Y == point.Y);

            int x;
            if (currentPoint == null)
            {
                x = point.X;
                point.IsConfirmed = true;
                m_points.Add(point);
            }
            else
            {
                x = currentPoint.X;
                currentPoint.IsConfirmed = true;
            }

            RaisePropertyChanged(nameof(ControllerGraph));

            //check is the last point
            if (x != LAST_POINT) return;
            //all points confirmed
            if (m_points.Any(controllerPoint => !controllerPoint.IsConfirmed)) throw new Exception("Ошибка подтверждения");
            IsGraphConfirmed = true;
            IsGraphReseted = false;
            IsControllerBusy = false;
        }
        /// <summary>
        /// Reset points 'x' to zero
        /// </summary>
        /// <param name="point"></param>
        public void ResetPoint(ControllerPoint point)
        {
            var currentPoint = m_points.FirstOrDefault(ovenPoint => ovenPoint.X == point.X);

            if (currentPoint == null) return;
            currentPoint.Y = 0;
            currentPoint.IsConfirmed = false;
            RaisePropertyChanged(nameof(ControllerGraph));

            //check is the last point
            if (currentPoint.X != LAST_POINT) return;
            // Any point does not have a 0 on 'Y'
            if (m_points.Any(controllerPoint => controllerPoint.Y != 0)) throw new Exception("Ошибка сброса");
            IsGraphReseted = true;
            IsControllerBusy = false;
        }

        public void Clear()
        {
            m_points.Clear();
            RaisePropertyChanged(nameof(ControllerGraph));
        }

        #endregion Methods

        #region Properties

        private bool m_isControllerBusy;
        public bool IsControllerBusy
        {
            get => m_isControllerBusy;
            set
            {
                m_isControllerBusy = value;
                RaisePropertyChanged();
            }
        }

        private bool m_isGraphConfirmed;
        public bool IsGraphConfirmed
        {

            get => m_isGraphConfirmed;
            set
            {
                if (m_isGraphConfirmed == value) return;
                m_isGraphConfirmed = value;
                RaisePropertyChanged();
            }
        }

        private bool m_isGraphReseted;
        public bool IsGraphReseted
        {
            get => m_isGraphReseted;
            set
            {
                if (m_isGraphReseted == value) return;
                m_isGraphReseted = value;
                RaisePropertyChanged();
            }
        }

        private readonly IList<ControllerPoint> m_points = new List<ControllerPoint>();
        public ReadOnlyCollection<ControlllerLine> ControllerGraph
        {
            get
            {
                var lines = new Collection<ControlllerLine>();

                for (var pointIndex = 1; pointIndex < m_points.Count; pointIndex++)
                {
                    var previosPoint = m_points[pointIndex - 1];
                    var point = m_points[pointIndex];

                    lines.Add(new ControlllerLine(previosPoint, point));
                }

                return new ReadOnlyCollection<ControlllerLine>(lines);
            }
        }

        #endregion Properties
    }
}