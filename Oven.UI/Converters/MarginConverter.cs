﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Oven.Converters
{
    internal class MarginConverter : IMultiValueConverter
    {
        private const ushort MAX_RIGHT = 440;
        private const ushort MAX_TOP = 310;

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var x = (int)values[0];
            var y = (int)values[1];

            var left = x >= MAX_RIGHT ? -75 : 10;   //shift info to left side
            var top = y >= MAX_TOP ? -40 : 10;  //shoft info to bottom side
            return new Thickness(left, top, 0, 0);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}