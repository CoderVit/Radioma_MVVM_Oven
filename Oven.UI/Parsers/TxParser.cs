﻿using Oven.Models.Points;
using System;
using System.Text.RegularExpressions;

namespace Oven.Parsers
{
    internal class TxParser : IParser<ControllerPoint>
    {
        private readonly Regex m_graphRegex = new Regex("(\\w)=([\\d]{1,3})");

        public ControllerPoint ParsePoint(string response)
        {
            var mathes = m_graphRegex.Matches(response);
            var x = Convert.ToInt32(mathes[0].Groups[2].Value);
            var y = Convert.ToInt32(mathes[1].Groups[2].Value);
            return new ControllerPoint(x, y);
        }
    }
}