﻿namespace Oven.Parsers
{
    internal interface IParser<out T>
    //where T : OvenPointBase
    {
        T ParsePoint(string response);
    }
}