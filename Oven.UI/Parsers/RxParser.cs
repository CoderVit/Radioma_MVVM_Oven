﻿using Oven.Models;
using Oven.Models.Points;
using System;
using System.Text.RegularExpressions;

namespace Oven.Parsers
{
    internal class RxParser : IParser<ProcessPoint>
    {
        private readonly Regex m_regex = new Regex("(\\w{1,2})=([\\w\\d]{1,3})");

        public ProcessPoint ParsePoint(string response)
        {
            var mathes = m_regex.Matches(response); //split string to mathes array

            var x = Convert.ToInt32(mathes[0].Groups[2].Value);
            var first = new ProcessTemperature(Convert.ToInt32(mathes[1].Groups[2].Value), mathes[2].Groups[2].Value == "Y");
            var second = new ProcessTemperature(Convert.ToInt32(mathes[3].Groups[2].Value), mathes[4].Groups[2].Value == "Y");
            var third = new ProcessTemperature(Convert.ToInt32(mathes[5].Groups[2].Value), mathes[6].Groups[2].Value == "Y");
            return new ProcessPoint(x, first, second, third);
        }
    }
}