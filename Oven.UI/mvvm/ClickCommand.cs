﻿using Oven.Annotations;
using Oven.Models;
using System;
using System.Windows;
using System.Windows.Input;
using Oven.Models.Points;

namespace Oven.mvvm
{
    internal class ClickCommand : ICommand
    {
        private readonly Action<DrawPoint> m_action;
        private readonly Predicate<object> m_canExecute;

        public ClickCommand([NotNull] Action<DrawPoint> action, [NotNull] Predicate<object> canExecute)
        {
            m_action = action ?? throw new ArgumentNullException(nameof(action));
            m_canExecute = canExecute ?? throw new ArgumentNullException(nameof(canExecute));
        }

        public ClickCommand(Action<DrawPoint> execute) : this(execute, _ => true) { }

        public ClickCommand(Action execute) : this(_ => execute(), _ => true) { }

        public void Execute(object parameter)
        {
            var mousePos = Mouse.GetPosition((IInputElement)parameter);
            m_action.Invoke(new DrawPoint((int)mousePos.X, (int)mousePos.Y));
        }

        public bool CanExecute(object parameter)
        {
            return m_canExecute == null || m_canExecute(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }
}