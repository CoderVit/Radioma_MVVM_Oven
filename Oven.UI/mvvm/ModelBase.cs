﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Oven.mvvm
{
    public class ModelBase : INotifyPropertyChanged
    {
        #region Notify property changed

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected void SetValue(ref object property, object value)
        {
            property.GetType().GetProperty(nameof(property)).SetValue(property, value);
            RaisePropertyChanged(nameof(property));
        }

        #endregion Notify property changed
    }
}