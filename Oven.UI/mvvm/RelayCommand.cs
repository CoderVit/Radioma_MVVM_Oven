﻿using Oven.Annotations;
using System;
using System.Windows.Input;

namespace Oven.mvvm
{
    internal class RelayCommand : ICommand
    {
        private readonly Action<object> m_action;
        private readonly Predicate<object> m_canExecute;

        public RelayCommand([NotNull] Action<object> paramAction, Predicate<object> canExecute)
        {
            m_action = paramAction ?? throw new ArgumentNullException(nameof(paramAction));
            m_canExecute = canExecute;
        }

        public RelayCommand(Action<object> execute) : this(execute, _ => true) { }

        public RelayCommand(Action execute) : this(_ => execute(), _ => true) { }

        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }

        public bool CanExecute(object parameter)
        {
            return m_canExecute == null || m_canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            m_action?.Invoke(parameter);
        }
    }
}
