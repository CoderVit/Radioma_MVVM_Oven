﻿using Oven.Extensions;
using Oven.Models.Points;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;

namespace Oven.Controls
{
    public partial class OvenDot
    {
        public static readonly DependencyProperty ControlParentProperty = DependencyProperty.Register("ControlParent", typeof(IInputElement), typeof(OvenDot), null);

        public IInputElement ControlParent
        {
            get => (IInputElement)GetValue(ControlParentProperty);
            set => SetValue(ControlParentProperty, value);
        }

        private bool m_isInDrag;

        public OvenDot()
        {
            InitializeComponent();
        }

        private void OvenDot_OnMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Context.OnDeleteInvoke?.Invoke(Context);
        }

        private void OvenDot_OnLeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;                     //prevent add new point over the another point
            if (!(sender is Ellipse element) || !Context.IsDraggable) return;
            element.CaptureMouse();
            m_isInDrag = true;
        }

        private void OvenDot_OnMouseMove(object sender, MouseEventArgs e)
        {
            if (!m_isInDrag) return;

            var currentPoint = (DrawPoint)e.GetPosition(ControlParent);

            if (!currentPoint.IsValid()) return;

            Context.SetX(currentPoint.X);
            Context.SetY(currentPoint.Y);
        }

        private void OvenDot_OnLeftMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!m_isInDrag || !(sender is Ellipse element)) return;

            element.ReleaseMouseCapture();
            m_isInDrag = false;
            e.Handled = true;
        }

        private DrawPoint Context => DataContext as DrawPoint;
    }
}